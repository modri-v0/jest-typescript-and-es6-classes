# Ejemplo de testeo con clases, Typescript y Jest

Este proyecto contiene ejemplos de como crear dobles de pruebas, comunmente llamados `mocks`, como alternativa a realizar `mocks` de módulos completos con `Jest`.

En el proyecto se muestra que es posible desacoplarse de la utilidad `mock` de `Jest` y utilizar las funcionalidades provistas por Typescript (o Javascript) directamente mediante inyección de dependencias (ver [Introducción](#introducción)) y tres estrategias:

- [Mockear metódo de instancia](#mockear-metódo-de-instancia-de-clases)
- [Utilizar un objeto con la misma interfaz](#utilizar-un-objeto-con-la-misma-interfaz-que-soundplayer)
- [Utilizar un objeto de una clase derivada de una clase abstracta](#utilizar-un-objeto-de-una-clase-derivada-de-una-clase-abstracta)

El código resultante se encuentra en el directorio [src](src/) y los tests en [test](test/) agrupados por estrategia. En el caso de los tests se muestra además distintas maneras de armar las pruebas sin usar las utilidades de `Jest`, usándolas a nivel objeto y usándolas a nivel módulo.

Por último se plantean algunas observaciones sobre el uso utilidades de `mocking` y algunas referencias para profundizar en el tema de pruebas de código y dobles de prueba en [Comentarios finales y profundizando en el tema](#comentarios-finales-y-profundizando-en-el-tema).

## Introducción

¿Para qué molestarse? ¿Para qué evitar utilizar `mock` de `Jest` sobre un módulo completo? Hay varios motivos, por ejemplo desacoplarse del framework o biblioteca de testing, porque la configuración suele ser engorrosa (ver [test con mock de clase](./test/testeable-using-concrete-class/sound-player-consumer-with-class-mock.test.ts)), para localizar el cambio en una instancia de clase y no afectar a toda una clase donde debe tenerse cuidado sobre el ciclo de vida del cambio para no afectar a otras pruebas, pero principalmente porque la alternativa en general ayuda a tener buenos diseños.

No quiere decir que las utilidades de los frameworks o bibliotecas de testings sean malas herramientas o esté mal usarlas, simplemente que muchas veces se utilizan como atajos o parches a problemas de diseño, por ejemplo acoplamiento.

El proyecto toma el [ejemplo de mocks de Jest sobre clases ES6](https://jestjs.io/docs/es6-class-mocks) que modela dos clases `SoundPlayerConsumer` y `SoundPlayer`. `SoundPlayer` es un colaborador de `SoundPlayerConsumer` que se utiliza en el método `playSomethingCool` de `SoundPlayerConsumer` como se puede observar en el siguiente código:

```typescript
// sound-player.ts
export default class SoundPlayer {
  constructor() {
    this.foo = 'bar';
  }

  playSoundFile(fileName) {
    console.log('Playing sound file ' + fileName);
  }
}

// sound-player-consumer.ts
import SoundPlayer from './sound-player';

export default class SoundPlayerConsumer {
  constructor() {
    this.soundPlayer = new SoundPlayer();
  }

  playSomethingCool() {
    const coolSoundFileName = 'song.mp3';
    this.soundPlayer.playSoundFile(coolSoundFileName);
  }
}
```

Como puede observarse hay un alto acoplamiento entre `SoundPlayer` y `SoundPlayerConsumer` al inicializarse `SoundPlayer` dentro del constructor `SoundPlayerConsumer`. Esto obliga, con el fin de realizar tests, a hacer uso de las utilidades de `Jest` o a modificar la propiedad `soundPlayer` de `SoundPlayerConsumer` luego de ser instanciada rompiendo encapsulamiento 

Para evitar esto, basta con desacoplar la inicialización `SoundPlayer` del constructor de `SoundPlayerConsumer`. Es decir, permitir la inyección de una instancia `SoundPlayer` al crear una instancia de `SoundPlayerConsumer`. Esto da mayor flexibilidad, permitiendo inyectar las instancias que se necesite para realizar las pruebas. 

El constructor de `SoundPlayerConsumer` queda entonces así:

```typescript
// sound-player-consumer.ts
import SoundPlayer from './sound-player';

export default class SoundPlayerConsumer {
    
  constructor(aSoundPlayer: SoundPlayer) {
    this.soundPlayer = aSoundPlayer;
  }

  // ... mas codigo
}
```

Se incluyen también alternativas a la inyección de una instancia concreta de `SoundPlayer`, como el uso de [interfaces](https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#interfaces) o [clases abstractas](https://www.typescriptlang.org/docs/handbook/2/classes.html#abstract-classes-and-members). Ambas alternativas se explican más abajo.

Para evitar que el cambio en el constructor afecte fuertemente al proyecto, una posibilidad es crear un método o función para la construcción `SoundPlayerConsumer` con la instancia `SoundPlayer` por defecto y llamarlo en reemplazo del constructor de la clase.

Por ejemplo implementándolo en `SoundPlayerConsumer` como método estático que en el siguiente ejemplo llamaremos `withDefaultSoundPlayer`.

```typescript
export default class SoundPlayerConsumer {

  static withDefaultSoundPlayer() {
    const soundPlayer = new SoundPlayer();
    return new SoundPlayerConsumer(soundPlayer);
  }
  // ... mas codigo
}

// Luego en lo llamados al constructor de SoundPlayerConsumer. Por ejemplo:

const aSoundPlayerConsumer = new SoundPlayerConsumer();

// Se cambia por

const aSoundPlayerConsumer = SoundPlayerConsumer.withDefaultSoundPlayer();

```

## Estrategias de implementación

Como se indicó al inicio, en el proyecto se muestran tres posibles estrategias para evitar utilizar la utilidad de `mock` de `Jest` a nivel módulo. En concreto para el ejemplo estas estrategias son:
- Mockear método de instancia de `SoundPlayer`
- Utilizar un objeto con la misma interfaz que `SoundPlayer`
- Utilizar un objeto de una clase derivada de una clase abstracta padre de `SoundPlayer`

A continuación se detallan cada una de ellas y algunas variantes de implementación.

### Mockear metódo de instancia de clases

Tal vez el más sencillo de los métodos es modificar el comportamiento de un método de un objeto a fin de ejercitar y probar el comportamiento del resto del código.

Se detallan a continuación tres maneras de lograr esto:

- [Cambiando la implementación del método en el objeto](#cambiando-la-implementación-del-método-para-la-instancia)
- [Utilizando `Proxy` para tener una nueva implementación del método](#utilizando-proxy-para-tener-una-nueva-implementación-del-método)
- [Utilizando `Jest` para cambiar la implementación del método](#utilizando-jest-para-crear-una-nueva-implementación-del-método)
#### Cambiando la implementación del método para la instancia 

Para implementar el comportamiento necesario para una prueba es posible modificar el método `playSoundFile` de una instancia de la clase `SoundPlayer`, por ejemplo, si se necesitara lanzar una excepción al llamar al método `playSoundFile` basta con modifcar el método de la siguiente manera:

```typescript
  mockSoundPlayerInstance.playSoundFile = function () {
    throw new Error('An error message');
  };
```

El ejemplo completo cambiando el método de la instancia para un caso exitoso y para uno que lanza una excepción puede verse [aqui](./test/testeable-using-concrete-class/sound-player-consumer.test.ts).

#### Utilizando Proxy para tener una nueva implementación del método

Si no se quisiera modificar la instancia, una posibilidad es utilizar un objeto [Proxy](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy) de JavaScript en lugar de la instancia de `SoundPlayer`. Al igual que en el ejemplo anterior si se quiere lanzar una excepción: 

```typescript  
  const proxiedPlaySoundFile =  function () {
    throw new Error('An error message');
  };

  const handler = {
      get(target, propertyName, receiver) {
        const propertyValue = Reflect.get(target, propertyName, receiver);
        if (typeof propertyValue === 'function') {
          if (propertyName === 'playSoundFile') {
            return proxiedPlaySoundFile.bind(target);
          }
          return propertyValue.bind(target);
        } else {
          return propertyValue;
        }
      },
    };
    const proxiedASoundPlayerInstance = new Proxy(
      aSoundPlayerInstance,
      handler
    );

    const soundPlayerConsumer = new SoundPlayerConsumer(
      proxiedASoundPlayerInstance
    );
```

Como puede verse el uso de `Proxy` vuelve más verbosos y complejos los tests, por lo que se necesita generar abstracciones adicionales para facilitar su lectura como puede verse en el ([ejemplo completo](./test/testeable-using-concrete-class/sound-player-consumer-with-Proxy.test.ts)), pero permite decorar o cambiar el comportamiento de un objeto sin cambiarlo.

La utilización de objetos `Proxy` puede en algunos casos ser muy útil, es lo que utiliza [ts-mockito](https://www.npmjs.com/package/@typestrong/ts-mockito) para crear el comportamiento de los dobles de pruebas. 

#### Utilizando Jest para cambiar la implementación del método
 
Otra opción es utilizar `Jest`. Si se espia con `jest.spyOn(...)` el método `playSoundFile` y luego al objeto devuelto se llama al método `mockImplementation` (o similares) pasándole como parámetro la nueva implementación. El código en ese caso quedaría así: 

```typescript
  const playSoundFileMethod = jest.spyOn(aSoundPlayerInstance, 'playSoundFile');
  playSoundFileMethod.mockImplementation(() => {
    throw new Error('An error message');
  }); 
```

El ejemplo completo con `Jest` puede verse [aqui](./test/testeable-using-concrete-class/sound-player-consumer-with-jestfn.test.ts).

#### Algunas observaciones

Las tres formas resuelven el problema, teniendo solo que modificar un objeto instancia de una clase. Una desventaja de `mockear` el método de la instancia es que la inicialización o construcción de instancias podría ser complicada o costosa. El costo de la inicialización de la clase puede evitarse con la utilización de las siguientes estrategias: usando de interfaces o clases abstractas.

### Utilizar un objeto con la misma interfaz que `SoundPlayer`

Typescript permite definir interfaces permitiéndonos pensar en objetos polimórficos a nivel método y no estrictamente en jerarquías de clases.

Para poder utilizar interfaces en el ejemplo, primero es necesario definir una interfaz que sea idéntica a la de `SoundPlayer`, es decir, una interfaz que defina el método `playSoundFile` con la misma signatura (parametros de entrada y valor retornado). Una vez definida, hay que modificar la clase `SoundPlayer` para indicar que implementa dicha interfaz y modificar el constructor de la clase `SoundPlayerConsumer` para que reciba un objeto que cumpla con dicha interfaz.

Si llamamos a la interfaz `SoundFilePlayer` entonces el código quedaría así: 
 
```typescript
// Definicion interfaz
interface SoundFilePlayer {
  playSoundFile(fileName: string);
}

// Clase concreta que implementa la interfaz
class SoundPlayer implements SoundFilePlayer {
// implementacion clase
}

class SoundPlayerConsumer {
  private soundPlayer: SoundFilePlayer;

  constructor(aSoundFilePlayer: SoundFilePlayer) {
    this.soundPlayer = aSoundFilePlayer;
  }
  // ... mas codigo
}
```

En los tests es posible entonces definir un objeto que cumpla con la interfaz (o una clase). Hay que tener en cuenta es que si se crea un objeto que cumpla la interfaz, Typescript no permitirá que tenga propiedades, ni métodos extra. En caso de necesitar definir propiedades o métodos extra, puede salvarse esto al menos de tres maneras: haciendo uso de `scopes`, extendiendo la interfaz o creando una clase de pruebas que extienda la interfaz.

Por ejemplo, si se quisiera validar el nombre del último archivo de canción reproducido por el objeto que implementa la interfaz, podría hacerse de las siguientes maneras:

```typescript
    // Haciendo uso del scope
    let lastFilename;
    const mockSoundFilePlayer: SoundFilePlayer = {
      playSoundFile: function (fileName: string) {
        lastFilename = fileName;
      },
    };
    // mas codigo del test
    // Validando la expectativa del último archivo reproducido
    expect(lastFilename).toBe('song.mp3'); 

    // Extendiendo la interfaz
    interface SoundFilePlayerWithLastFilename extends SoundFilePlayer {
      lastFilename: string | undefined;
    }
    const mockSoundFilePlayer: SoundFilePlayerWithLastFilename = {
      lastFilename: undefined,
      playSoundFile: function (fileName: string) {
        this.lastFilename = fileName;
      },
    };
    // mas codigo del test
    // Validando la expectativa del último archivo reproducido
    expect(mockSoundFilePlayer.lastFilename).toBe('song.mp3');

    // Implementando una clase de pruebas
    class TestSoundFilePlayer implements SoundFilePlayer {
      lastFilename: string | undefined;

      playSoundFile(fileName: any) {
        this.lastFilename = fileName;
      }
    }
    // En el tests
    const mockSoundPlayerInstance = new TestSoundFilePlayer();

    // mas codigo del test
    // Validando la expectativa del último archivo reproducido
    expect(mockSoundFilePlayer.lastFilename).toBe('song.mp3');
```

Los ejemplos completos de los casos con objetos planos pueden encontrarse [aquí](./test/testeable-using-interfaces/sound-player-consumer-with-plain-object.test.ts) y uno utilizando con una clase de pruebas que implementa la interfaz puede verse [aquí](./test/testeable-using-interfaces/sound-player-consumer-with-test-class-implementing-interface.test.ts).

Alternativamente se puede utilizar `jest.Mock` para implementar el comportamiento del método. Esto permite variar fácilmente el comportamiento de los objetos y realizar verificaciones.

```typescript

  // En el setup del test
  const playSoundFileMethod: jest.Mock = jest.fn();
  const mockSoundFilePlayer: SoundFilePlayer = {
    playSoundFile: playSoundFileMethod,
  };

  // En un test implementando lanzar una expepción
  playSoundFileMethod.mockImplementation(() => {
    throw new Error('Mocked error!');
  });

  // Verificando que se lanza la excepción
  expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(
    'Mocked error!'
  );

  // Otro test donde se verifica el nombre del último archivo reproducido
  expect(mockSoundFilePlayer.playSoundFile).toHaveBeenCalledWith(
      'song.mp3'
  );
```

El ejemplo completo utilizando `jest.fn()` se encuentra [aquí](./test/testeable-using-interfaces/sound-player-consumer-with-jestfn.test.ts).

Si se utilizan objetos planos que implementan la interfaz (o una extensión de la misma), sea que se utilice o no las utilidades del framework o biblioteca de tests para generar comportamiento y verificación, se debe tener cuidado de no terminar con código repetido con la inicialización de dichos objetos. Para evitar esto conviene ir refactorizando el código de los tests, incluso reificar estos objetos de prueba en una clase de pruebas que cumpla con la interfaz. Algunas ideas de reificación pueden verse a continuación.

### Utilizar un objeto de una clase derivada de una clase abstracta

Typescript también permite definir [clases abstractas](https://www.typescriptlang.org/docs/handbook/2/classes.html#abstract-classes-and-members), es decir, clases cuya implementación completa queda en las clases derivadas de esta. Esto puede ser conveniente cuando hay código común entre las clases derivadas que incluso podría utilizarse en las pruebas.

Continuando el ejemplo para implementar una clase abstracta, de manera similar al caso de la interfaz, hay que definir la clase abstracta con el método `playSoundFile` definido como método abstracto, hacer que `SoundPlayer` extienda de `SoundFilePlayer` y que el constructor de `SoundPlayerConsumer` reciba un objeto de la clase abstracta en lugar de la clase concreta `SoundPlayer`.

El código queda entonces de esta manera:

```typescript
// Definicion interfaz
abstract class SoundFilePlayer {
  abstract playSoundFile(fileName: string);
}

// Clase concreta que implementa la interfaz
class SoundPlayer extends SoundFilePlayer {
// implementacion clase
}

class SoundPlayerConsumer {
  private soundPlayer: SoundFilePlayer;

  constructor(aSoundFilePlayer: SoundFilePlayer) {
    this.soundPlayer = aSoundFilePlayer;
  }
  // ... mas codigo
}
```
Al momento de realizar pruebas al usar clases abstractas una opción es generar clases derivadas para las pruebas, de manera similar al caso de implementar la interfaz con una clase de pruebas. Por ejemplo:

```typescript
// Clase concreta que implementa la interfaz
class TestSoundFilePlayer extends SoundFilePlayer {

   playSoundFile(fileName: string) {
      throw new Error('Mocked error!');
    }
}
```

Podría darse la situación en que necesitemos poder variar fácilmente el comportamiento de `playSoundFile` dependiendo del test (ojo de no caer en la sobreingeniería innecesaria). Como vimos anteriormente, podríamos reemplazar el método sobre la instancia construida en la prueba o usar `jest.fn`. Otra alternativa podría ser parametrizar el comportamiento en la construcción de la instancia pasándole la implementación de la función o una `jest.fn`. También podría inicializarse una `jest.fn` como una propiedad pública que luego se llama dentro de `playSoundFile`.

```typescript
  // Clase concreta de pruebas que implementa la interfaz
  // que recibe una función en el constructor
  class TestSoundFilePlayer extends SoundFilePlayer {
    playSoundFileImplementation: (fileName: string)=>void;

    constructor(playSoundFileImplementation: (fileName: string)=>void){
      super();
      this.playSoundFileImplementation = playSoundFileImplementation;
    }

    playSoundFile(fileName: string) {
        this.playSoundFileImplementation(fileName);
      }
  }
  const errorMessage = 'Mocked error!';
  const playSoundFileWithError = ()=> {throw new Error(errorMessage)};
  const aTestSoundFilePlayer = new TestSoundFilePlayer(playSoundFileWithError);
  const soundPlayerConsumer = new SoundPlayerConsumer(
    aTestSoundFilePlayer
  );

  expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(
    errorMessage
  );

  // Clase concreta de pruebas que implementa la interfaz
  // que expone una propiedad del tipo jest.Mock 
  class TestSoundFilePlayer extends SoundFilePlayer {
    this.playSoundFileImplementation: jest.Mock;

    constructor(){
      super();
      this.playSoundFileImplementation = jest.fn();
    }

    playSoundFile(fileName: string) {
        this.playSoundFileImplementation(fileName);
      }
  }
  const errorMessage = 'Mocked error!';
  const mockSoundFilePlayer = new TestSoundFilePlayer();
  mockSoundFilePlayer.playSoundFileImplementation.mockImplementation(() => {
      throw new Error(errorMessage);
    });
  const soundPlayerConsumer = new SoundPlayerConsumer(
    mockSoundFilePlayer
  );

  expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(
    errorMessage
  );

```

En este caso la opción de exponer una propiedad del tipo `jest.Mock` parece ser la más breve y bastante pragmática, pero tiene un fuerte acoplamiento con `Jest` (o la utilidad de doble de pruebas elegida).

El ejemplo completo utilizando una clase derivada sin `jest.fn` puede encontrarse [aquí](test/testeable-using-abstract-class/sound-player-consumer.test.ts) y [aquí](test/testeable-using-abstract-class/sound-player-consumer-with-jestfn.test.ts) la versión que utiliza `jest.fn`.

Como se mencionó anteriormente, utilizar las utilidades de `mocks` puede ser la manera más rápida de avanzar a escribir tests, pero cada proyecto tiene distintos escenarios de tests e ir refactorizando la clase de pruebas nos puede ayudar a entender que esperamos de los objetos que lo utilizan y las cosas queremos verificar en los tests. Este entendimiento nos puede ayudar en mejorar el código del proyecto en general.
## Comentarios finales y profundizando en el tema

Como se ve es posible generar dobles de pruebas sin necesidad de utilidades externas. Y si bien es muy fácil utilizar los frameworks o bibliotecas de pruebas que facilitan generar dobles de pruebas que permitan verificar llamadas o generar comportamiento test a test, como es el caso de `Jest`, esta facilidad puede no ayudarnos a construir código testeable. El ejemplo provisto por `Jest` con clases es muy bueno para mostrar como funciona la utilidad `mock`, pero no es un buen ejemplo de código testeable. 

En sí ese ejemplo es un buen recordatorio que las bibliotecas y frameworks quieren que las utilicemos y mostrarán de que manera facilitan el desarrollo de código a nivel general, pero eso no quiere decir que faciliten nuestros desarrollos, ni su mantenimiento y también es bueno recordar que no hay garantías de que continúen existiendo dentro de algunos años. Con esto no quiero decir que no haya que utilizar bibliotecas y frameworks, si no de sopesar sus beneficios y siempre que sea posible minimizar el acoplamiento a estas.

Y si bien comúnmente llamamos `mocks` a los dobles de pruebas hay toda una categorización que puede verse resumida en este [artículo de Martin Fowler](https://www.martinfowler.com/bliki/TestDouble.html). Fowler también escribió el artículo ["Mocks aren't stubs"](https://martinfowler.com/articles/mocksArentStubs.html) que profundiza sobre el uso de `mocks` y sobre sus pros y contras. 

James Shore en su [artículo sobre como testear sin `mocks`](https://www.jamesshore.com/v2/projects/testing-without-mocks/testing-without-mocks) muestra como hacer código testeable y buenas pruebas de código sin utilizar mocks. En cuanto a la creación de instancias para pruebas Shore utiliza fuertemente [null object pattern](https://en.wikipedia.org/wiki/Null_object_pattern).

En lo personal prefiero el uso de lo que yo llamo implementaciones sencillas, Fowler les llamaría `fakes`, en vez de utilizar `mocks`. Estos objetos puede inicializarse con información que se encuentra en archivos que luego puede consultarse sin necesidad de ir a recursos externos o guardando temporalmente la información en memoria. Estas implementaciones sencillas se pueden utilizar para construir una versión inicial desplegable y que permita verificar ciertas integraciones y comportamientos esperados. A medida que se va desarrollando estas implementaciones se van reemplazando por las implementaciones finales. Hernan Wilkinson muestra en distintos episodios de [Diseño a la Gorra](https://academia.10pines.com/disenio_a_la_gorra) el uso de clases que guardan información en memoria o trasientes, así como hacer buen uso de definición de interfaces y de objetos polimórficos para reemplazar distintas implementaciones.

Por último el artículo de Hillel Wayne ["In defense of testing mocks"](https://buttondown.email/hillelwayne/archive/in-defense-of-testing-mocks/) como su título lo indica sale en defensa de uso de `mocks` en los tests o más bien hace un análisis en que casos pueden ser de utilidad usarlos.