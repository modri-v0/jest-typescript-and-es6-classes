/**
 * SoundFilePlayer interface
 */
export interface SoundFilePlayer {
  playSoundFile(fileName);
}

/**
 * SoundPlayer class
 */
export default class SoundPlayer implements SoundFilePlayer {
  /**
   * Plays a sound file
   * @param {string} fileName fileName to play
   */
  playSoundFile(fileName) {
    console.log('Playing sound file ' + fileName);
  }
}
