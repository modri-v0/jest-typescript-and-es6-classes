/**
 * SoundFilePlayer interface
 */
export abstract class SoundFilePlayer {
  abstract playSoundFile(fileName: string);
}

/**
 * SoundPlayer class
 */
export default class SoundPlayer implements SoundFilePlayer {
  /**
   * Plays a sound file
   * @param {string} fileName fileName to play
   */
  playSoundFile(fileName: string) {
    console.log('Playing sound file ' + fileName);
  }
}
