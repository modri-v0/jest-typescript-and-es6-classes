import SoundPlayer from './sound-player.js';

/**
 * SoundPlayerConsumer
 */
export default class SoundPlayerConsumer {
  private soundPlayer: SoundPlayer;

  /**
   * Construct an instance with a SoundPlayer instance
   * @return {SoundPlayerConsumer} a new instance
   */
  static withDefaultSoundPlayer() {
    const soundPlayer = new SoundPlayer();
    return new SoundPlayerConsumer(soundPlayer);
  }

  /**
   * Constructor
   * @param {SoundFilePlayer} aSoundFilePlayer instance
   */
  constructor(aSoundFilePlayer: SoundPlayer) {
    this.soundPlayer = aSoundFilePlayer;
  }

  /**
   * Method
   */
  playSomethingCool() {
    const coolSoundFileName = 'song.mp3';
    this.soundPlayer.playSoundFile(coolSoundFileName);
  }
}
