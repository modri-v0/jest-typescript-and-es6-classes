/**
 * SoundPlayer class
 */
export default class SoundPlayer {
  /**
   * Plays a sound file
   * @param {string} fileName fileName to play
   */
  playSoundFile(fileName) {
    console.log('Playing sound file ' + fileName);
  }
}
