import SoundPlayer from './sound-player.js';

/**
 * SoundPlayerConsumer
 */
export default class SoundPlayerConsumer {
  private soundPlayer: SoundPlayer;

  /**
   * Constructor
   */
  constructor() {
    this.soundPlayer = new SoundPlayer();
  }

  /**
   * Method
   */
  playSomethingCool() {
    const coolSoundFileName = 'song.mp3';
    this.soundPlayer.playSoundFile(coolSoundFileName);
  }
}
