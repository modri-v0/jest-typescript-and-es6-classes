import SoundPlayerConsumer from '../../src/testeable-using-abstract-class/sound-player-consumer.js';
import { SoundFilePlayer } from '../../src/testeable-using-abstract-class/sound-player.js';

/**
 * Mock class for test
 */
class TestSoundPlayer extends SoundFilePlayer {
  lastFilename: string | undefined;

  playSoundFile(fileName: any) {
    this.lastFilename = fileName;
  }
}
describe('Testeable SoundPlayerConsumer Derived Abstract Class Test case', () => {
  let mockSoundPlayerInstance: TestSoundPlayer;

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    mockSoundPlayerInstance = new TestSoundPlayer();
  });

  it('We can check if the consumer called a method on the class instance', () => {
    const soundPlayerConsumer = new SoundPlayerConsumer(
      mockSoundPlayerInstance
    );
    const coolSoundFileName = 'song.mp3';
    soundPlayerConsumer.playSomethingCool();

    expect(mockSoundPlayerInstance).not.toBeUndefined();
    expect(mockSoundPlayerInstance.lastFilename).toBe(coolSoundFileName);
  });

  it('We can check throw an error on call', () => {
    const errorMessage = 'Mocked error!';
    const soundPlayerConsumer = new SoundPlayerConsumer(
      mockSoundPlayerInstance
    );
    mockSoundPlayerInstance.playSoundFile = function () {
      throw new Error(errorMessage);
    };

    expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(errorMessage);
  });
});
