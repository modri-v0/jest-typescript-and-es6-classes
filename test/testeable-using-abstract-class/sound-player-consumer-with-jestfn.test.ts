import { jest } from '@jest/globals';
import SoundPlayerConsumer from '../../src/testeable-using-abstract-class/sound-player-consumer.js';
import { SoundFilePlayer } from '../../src/testeable-using-abstract-class/sound-player.js';

/**
 * Mock class for test
 */
class TestSoundPlayerWithJestFn extends SoundFilePlayer {
  playSoundFileMock: jest.Mock;

  /**
   * Constructor
   */
  constructor() {
    super();
    this.playSoundFileMock = jest.fn();
  }

  /**
   * Method
   * @param {string} fileName
   */
  playSoundFile(fileName: string) {
    this.playSoundFileMock(fileName);
  }
}
describe('Testeable SoundPlayerConsumer Derived Abstract Class Test case', () => {
  let mockSoundPlayerInstance: TestSoundPlayerWithJestFn;

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    mockSoundPlayerInstance = new TestSoundPlayerWithJestFn();
  });

  it('We can check if the consumer called a method on the class instance', () => {
    const soundPlayerConsumer = new SoundPlayerConsumer(
      mockSoundPlayerInstance
    );
    const coolSoundFileName = 'song.mp3';
    soundPlayerConsumer.playSomethingCool();

    expect(mockSoundPlayerInstance).not.toBeUndefined();
    expect(mockSoundPlayerInstance.playSoundFileMock).toHaveBeenCalledWith(
      coolSoundFileName
    );
    expect(mockSoundPlayerInstance.playSoundFileMock).toHaveBeenCalledTimes(1);
  });

  it('We can check throw an error on call', () => {
    const soundPlayerConsumer = new SoundPlayerConsumer(
      mockSoundPlayerInstance
    );
    mockSoundPlayerInstance.playSoundFileMock.mockImplementation(() => {
      throw new Error('Mocked error!');
    });

    expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(
      'Mocked error!'
    );
  });
});
