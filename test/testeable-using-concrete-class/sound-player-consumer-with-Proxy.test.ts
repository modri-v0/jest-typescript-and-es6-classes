import SoundPlayerConsumer from '../../src/testeable-using-concrete-class/sound-player-consumer.js';
import SoundPlayer from '../../src/testeable-using-concrete-class/sound-player.js';

describe('Testeable SoundPlayerConsumer Concrete Spy Method Test case', () => {
  /**
   * Creates a proxy to a SoundPlayer instance
   * replacing playSoundFile method with proxiedPlaySoundFile
   *
   * @param {(songfile: any)=>void} proxiedPlaySoundFile the method to use instead of proxiedPlaySoundFile
   * @param {SoundPlayer} aSoundPlayerInstance the instance to proxy
   * @returns {SoundPlayer} the proxy instance
   */
  function createSoundPlayerProxy(
    proxiedPlaySoundFile: (songfile: any) => void,
    aSoundPlayerInstance: SoundPlayer
  ): SoundPlayer {
    const handler = {
      get(target, propertyName, receiver) {
        const propertyValue = Reflect.get(target, propertyName, receiver);
        if (typeof propertyValue === 'function') {
          if (propertyName === 'playSoundFile') {
            return proxiedPlaySoundFile.bind(target);
          }
          return propertyValue.bind(target);
        } else {
          return propertyValue;
        }
      },
    };
    const proxiedASoundPlayerInstance = new Proxy<SoundPlayer>(
      aSoundPlayerInstance,
      handler
    );
    return proxiedASoundPlayerInstance;
  }

  let aSoundPlayerInstance: SoundPlayer;

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    aSoundPlayerInstance = new SoundPlayer();
  });

  it('We can check if the consumer called a method on the class instance', () => {
    let lastSong;
    const proxiedPlaySoundFile = function (songfile: any) {
      lastSong = songfile;
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      // eslint-disable-next-line no-invalid-this
      this.playSoundFile(songfile); // H
    };
    const proxiedASoundPlayerInstance = createSoundPlayerProxy(
      proxiedPlaySoundFile,
      aSoundPlayerInstance
    );

    const soundPlayerConsumer = new SoundPlayerConsumer(
      proxiedASoundPlayerInstance
    );
    const coolSoundFileName = 'song.mp3';

    soundPlayerConsumer.playSomethingCool();

    expect(lastSong).toBe(coolSoundFileName);
  });

  it('We can check throw an error on call', () => {
    const errorMessage = 'Mocked error!';

    const proxiedPlaySoundFile = function () {
      throw new Error(errorMessage);
    };
    const proxiedASoundPlayerInstance = createSoundPlayerProxy(
      proxiedPlaySoundFile,
      aSoundPlayerInstance
    );
    const soundPlayerConsumer = new SoundPlayerConsumer(
      proxiedASoundPlayerInstance
    );

    expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(errorMessage);
  });
});
