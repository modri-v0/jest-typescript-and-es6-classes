import SoundPlayerConsumer from '../../src/testeable-using-concrete-class/sound-player-consumer.js';
import SoundPlayer from '../../src/testeable-using-concrete-class/sound-player.js';

describe('Testeable SoundPlayerConsumer Concrete Spy Method Test case', () => {
  let aSoundPlayerInstance: SoundPlayer;

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    aSoundPlayerInstance = new SoundPlayer();
  });

  it('We can check if the consumer called a method on the class instance', () => {
    const soundPlayerConsumer = new SoundPlayerConsumer(aSoundPlayerInstance);
    const coolSoundFileName = 'song.mp3';
    let lastSong;
    aSoundPlayerInstance.playSoundFile = (soundfile: any) => {
      lastSong = soundfile;
    };
    soundPlayerConsumer.playSomethingCool();

    expect(lastSong).toBe(coolSoundFileName);
  });

  it('We can check throw an error on call', () => {
    const errorMessage = 'Mocked error!';
    const soundPlayerConsumer = new SoundPlayerConsumer(aSoundPlayerInstance);
    aSoundPlayerInstance.playSoundFile = () => {
      throw new Error(errorMessage);
    };

    expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(errorMessage);
  });
});
