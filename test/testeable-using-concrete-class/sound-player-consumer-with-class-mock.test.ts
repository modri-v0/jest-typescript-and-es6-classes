import { jest } from '@jest/globals';
import type SoundPlayer from '../../src/testeable-using-concrete-class/sound-player.js';

jest.unstable_mockModule(
  '../../src/testeable-using-concrete-class/sound-player.js',
  () => ({
    default: jest.fn().mockImplementation(() => {
      return {
        playSoundFile: jest.fn(),
      };
    }),
  })
);
describe('Testeable SoundPlayerConsumer Concrete Class Mock Test case', () => {
  let SoundPlayerConsumer;
  let SoundPlayerMock;
  beforeAll(async () => {
    const SoundPlayerConsumerModule = await import(
      '../../src/testeable-using-concrete-class/sound-player-consumer.js'
    );
    SoundPlayerConsumer = SoundPlayerConsumerModule.default;
    const SoundPlayerModule = await import(
      '../../src/testeable-using-concrete-class/sound-player.js'
    );
    SoundPlayerMock = SoundPlayerModule.default;
  });

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    (SoundPlayerMock as jest.MockedClass<typeof SoundPlayerMock>).mockClear();
  });

  it('We can check if the consumer called the class constructor', () => {
    const soundPlayerConsumer = SoundPlayerConsumer.withDefaultSoundPlayer();
    expect(soundPlayerConsumer).toBeInstanceOf(SoundPlayerConsumer);
    expect(SoundPlayerMock).toHaveBeenCalledTimes(1);
  });

  it('We can check if the consumer called a method on the class instance', () => {
    // Show that mockClear() is working:
    expect(SoundPlayerMock).not.toHaveBeenCalled();

    const soundPlayerConsumer = SoundPlayerConsumer.withDefaultSoundPlayer();
    // Constructor should have been called again:
    expect(SoundPlayerMock).toHaveBeenCalledTimes(1);

    const coolSoundFileName = 'song.mp3';
    soundPlayerConsumer.playSomethingCool();

    // mock.instances is available with automatic mocks:
    const mockSoundPlayer = (
      SoundPlayerMock as jest.MockedClass<typeof SoundPlayerMock>
    ).mock;

    // using result because instances are a dead end
    const result = mockSoundPlayer.results[0]!;
    const mockSoundPlayerInstance = result.value as SoundPlayer;

    expect(mockSoundPlayerInstance).not.toBeUndefined();
    // Equivalent to above check:
    expect(mockSoundPlayerInstance.playSoundFile).toHaveBeenCalledWith(
      coolSoundFileName
    );
    expect(mockSoundPlayerInstance.playSoundFile).toHaveBeenCalledTimes(1);
  });

  it('We can check throw an error on call', () => {
    const soundPlayerConsumer = SoundPlayerConsumer.withDefaultSoundPlayer();
    // Constructor should have been called again:
    expect(SoundPlayerMock).toHaveBeenCalledTimes(1);

    const mockSoundPlayer = (
      SoundPlayerMock as jest.MockedClass<typeof SoundPlayerMock>
    ).mock;

    // using result because instances are a dead end
    const result = mockSoundPlayer.results[0]!;
    const mockSoundPlayerInstance = result.value as SoundPlayer;
    (
      mockSoundPlayerInstance.playSoundFile as jest.MockedFunction<
        typeof SoundPlayerMock.playSoundFile
      >
    ).mockImplementation(() => {
      throw new Error('Mocked error!');
    }); // Alternative could be jest.fn() to variable on line 7

    expect(mockSoundPlayerInstance).not.toBeUndefined();

    expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(
      'Mocked error!'
    );
  });
});
