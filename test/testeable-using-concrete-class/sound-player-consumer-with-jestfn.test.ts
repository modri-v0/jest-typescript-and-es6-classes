import { jest } from '@jest/globals';
import SoundPlayerConsumer from '../../src/testeable-using-concrete-class/sound-player-consumer.js';
import SoundPlayer from '../../src/testeable-using-concrete-class/sound-player.js';

describe('Testeable SoundPlayerConsumer Concrete Spy Method Test case', () => {
  let aSoundPlayerInstance: SoundPlayer;
  let playSoundFileMethod;

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    aSoundPlayerInstance = new SoundPlayer();
    playSoundFileMethod = jest.spyOn(aSoundPlayerInstance, 'playSoundFile');
  });

  it('We can check if the consumer called a method on the class instance', () => {
    const soundPlayerConsumer = new SoundPlayerConsumer(aSoundPlayerInstance);
    const coolSoundFileName = 'song.mp3';
    soundPlayerConsumer.playSomethingCool();

    expect(aSoundPlayerInstance).not.toBeUndefined();
    expect(playSoundFileMethod).toHaveBeenCalledWith(coolSoundFileName);
    expect(playSoundFileMethod).toHaveBeenCalledTimes(1);
  });

  it('We can check throw an error on call', () => {
    const soundPlayerConsumer = new SoundPlayerConsumer(aSoundPlayerInstance);
    playSoundFileMethod.mockImplementation(() => {
      throw new Error('Mocked error!');
    });

    expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(
      'Mocked error!'
    );
  });
});
