import SoundPlayerConsumer from '../../src/testeable-using-interfaces/sound-player-consumer.js';
import type { SoundFilePlayer } from '../../src/testeable-using-interfaces/sound-player.js';

describe('Testeable SoundPlayerConsumer Mock Interface Test case', () => {
  it('[Not changing interface] We can check if the consumer called a method on the class instance', () => {
    let lastFilename;
    const mockSoundFilePlayer: SoundFilePlayer = {
      playSoundFile: function (fileName: string) {
        lastFilename = fileName;
      },
    };

    const soundPlayerConsumer = new SoundPlayerConsumer(mockSoundFilePlayer);
    const coolSoundFileName = 'song.mp3';
    soundPlayerConsumer.playSomethingCool();

    expect(mockSoundFilePlayer).not.toBeUndefined();
    expect(lastFilename).toBe(coolSoundFileName);
  });

  it('[Extending interface] We can check if the consumer called a method on the class instance', () => {
    interface SoundFilePlayerWithLastFilename extends SoundFilePlayer {
      lastFilename: string | undefined;
    }
    const mockSoundFilePlayer: SoundFilePlayerWithLastFilename = {
      lastFilename: undefined,
      playSoundFile: function (fileName: string) {
        this.lastFilename = fileName;
      },
    };

    const soundPlayerConsumer = new SoundPlayerConsumer(mockSoundFilePlayer);
    const coolSoundFileName = 'song.mp3';
    soundPlayerConsumer.playSomethingCool();

    expect(mockSoundFilePlayer).not.toBeUndefined();
    expect(mockSoundFilePlayer.lastFilename).toBe(coolSoundFileName);
  });

  it('We can check throw an error on call', () => {
    const errorMessage = 'Mocked error!';
    const mockSoundFilePlayer: SoundFilePlayer = {
      playSoundFile: function () {
        throw new Error(errorMessage);
      },
    };
    const soundPlayerConsumer = new SoundPlayerConsumer(mockSoundFilePlayer);

    expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(errorMessage);
  });
});
