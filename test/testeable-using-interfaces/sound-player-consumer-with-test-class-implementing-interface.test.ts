import SoundPlayerConsumer from '../../src/testeable-using-interfaces/sound-player-consumer.js';
import type { SoundFilePlayer } from '../../src/testeable-using-interfaces/sound-player.js';

/**
 * SoundFilePlayer for tests
 */
class TestSoundFilePlayer implements SoundFilePlayer {
  lastFilename: string | undefined;

  playSoundFile(fileName: any) {
    this.lastFilename = fileName;
  }
}

describe('Testeable SoundPlayerConsumer Mock Interface Test case', () => {
  let mockSoundPlayerInstance: TestSoundFilePlayer;

  beforeEach(() => {
    mockSoundPlayerInstance = new TestSoundFilePlayer();
  });

  it('We can check if the consumer called a method on the class instance', () => {
    const soundPlayerConsumer = new SoundPlayerConsumer(
      mockSoundPlayerInstance
    );
    const coolSoundFileName = 'song.mp3';
    soundPlayerConsumer.playSomethingCool();

    expect(mockSoundPlayerInstance).not.toBeUndefined();
    expect(mockSoundPlayerInstance.lastFilename).toBe(coolSoundFileName);
  });
  it('We can check throw an error on call', () => {
    const errorMessage = 'Mocked error!';
    const soundPlayerConsumer = new SoundPlayerConsumer(
      mockSoundPlayerInstance
    );
    mockSoundPlayerInstance.playSoundFile = function () {
      throw new Error(errorMessage);
    };

    expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(errorMessage);
  });
});
