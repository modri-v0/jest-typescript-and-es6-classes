import { jest } from '@jest/globals';
import SoundPlayerConsumer from '../../src/testeable-using-interfaces/sound-player-consumer.js';
import type { SoundFilePlayer } from '../../src/testeable-using-interfaces/sound-player.js';

describe('Testeable SoundPlayerConsumer Mock Interface Test case', () => {
  let mockSoundPlayerInstance: SoundFilePlayer;
  let playSoundFileMethod: jest.Mock;

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    playSoundFileMethod = jest.fn();
    mockSoundPlayerInstance = {
      playSoundFile: playSoundFileMethod,
    };
  });

  it('We can check if the consumer called a method on the class instance', () => {
    const soundPlayerConsumer = new SoundPlayerConsumer(
      mockSoundPlayerInstance
    );
    const coolSoundFileName = 'song.mp3';
    soundPlayerConsumer.playSomethingCool();

    expect(mockSoundPlayerInstance).not.toBeUndefined();
    expect(mockSoundPlayerInstance.playSoundFile).toHaveBeenCalledWith(
      coolSoundFileName
    );
    expect(mockSoundPlayerInstance.playSoundFile).toHaveBeenCalledTimes(1);
  });
  it('We can check throw an error on call', () => {
    const soundPlayerConsumer = new SoundPlayerConsumer(
      mockSoundPlayerInstance
    );
    playSoundFileMethod.mockImplementation(() => {
      throw new Error('Mocked error!');
    });

    expect(() => soundPlayerConsumer.playSomethingCool()).toThrow(
      'Mocked error!'
    );
  });
});
